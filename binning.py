import io
import os
import shutil
def concat(x):
	temp = ""
	for i in x:
		temp = temp + i
	return temp

count = 0
path = input("enter output of preprocessing")
binPath = input("Enter where you want your bins")
files = os.fsencode(path)
name = []
width = []


#get output files that are a csv
if not os.path.exists(binPath):
    os.makedirs(binPath)
for file in os.listdir(files):
	if ".csv" in os.fsdecode(file):
		name.append(path + os.fsdecode(file))


#find header files
for i in range(0, len(name)):
	x = open(name[i], "r")
	j = (x.readline()).split(",")
	
	#if it is a header file
	if int(j[7]) == 1:
		count +=1
		
		#what number chunk is it
		number = str(concat(list(filter(str.isdigit, name[i]))))
		if not os.path.exists(binPath + "/" + str(count)):
			os.makedirs(binPath + "/" + str(count))
		os.rename(name[i], binPath +"/" + str(count) + "/" + number + ".csv")
		os.rename(path + number + ".byte", binPath + "/" + str(count) + "/" + number + ".byte")
		os.rename(path + number + ".bgr", binPath + "/" + str(count) + "/" + number + ".bgr")
		width.append(j[2])


#find every chunk with width that fits into k bin
for i in range(0, len(name)):
	x = open(name[i], "r")
	j = (x.readline()).split(",")
	number = str(concat(list(filter(str.isdigit, name[i]))))
	for k in range(0, len(width)):
		if int(j[2]) == width[k]:
			shutil.copyfile(name[i], binPath +"/" + str(k+1) + "/" + number + ".csv")
			shutil.copyfile(path + number + ".byte", binPath + "/" + str(k+1) + "/" + number + ".byte")
			shutil.copyfile(path + number + ".bgr", binPath + "/" + str(k+1) + "/" + number + ".bgr")
			
#write widths to csv for debugging
final = open(binPath + "/widths.csv", "w")
for i in width:
	final.write(i + ",")
print("done")
