import io
import os
import math

def ced(path):
	print(path)
	name = []
	width = 0
	output = open(path[:-1] + ".csv", "w")
	for file in os.listdir(path):
		if ".csv" in os.fsdecode(file):
			name.append(path + os.fsdecode(file))
	for i in range(0, len(name)):
		x = open(name[i], "r")
		j = (x.readline()).split(",")
		
		#if it is a header file
		if int(j[7]) == 1:
			width = int(j[2])
	width_bgr = width * 3
	name = []
	for file in os.listdir(path):
		if ".bgr" in os.fsdecode(file) or ".rgb" in os.fsdecode(file):
			name.append(path + os.fsdecode(file))
	#loops through every permutation
	for k in range(0, len(name)):
		for j in range(0, len(name)):
			if k == j:
				continue
			file1 = open(name[k], "r")
			file2 = open(name[j], "r")
			rgb1 = file1.readline()
			rgb2 = file2.readline()
			rgblist1 = rgb1.split(",")
			rgblist2 = rgb2.split(",")
			file1.close()
			file2.close()

			if(rgblist1[-1] == ""):
				del rgblist1[-1]
			if(rgblist2[-1] == ""):
				del rgblist2[-1]

			# Taken from f1
			last_row = rgblist1[len(rgblist1)-width_bgr: ]
			second_last_row = rgblist1[len(rgblist1)-(width_bgr*2) : len(rgblist1)-width_bgr]

			#Taken from f2
			first_row = rgblist2[:width_bgr]

			# Calculating ED-nearby
			sum = 0
			for i in range(0, width_bgr):
				sum = sum + (float(last_row[i]) - float(second_last_row[i]))**2
			ed_nearby = math.sqrt(sum) / width

			# Calculating ED-boundary
			sum = 0
			for i in range(0, width_bgr):
				sum = sum + (float(last_row[i]) - float(first_row[i]))**2
			ed_boundary = math.sqrt(sum) / width

			ced = abs(ed_boundary - ed_nearby)
			output.write(name[k] + "," + name[j]+ "," + str(ced) + "\n")
	output.close()

#main
binPath = input("Enter bin path")
for files in os.listdir(binPath):
	if os.path.isdir(os.path.join(binPath, files)):
		ced(binPath + "/" +  os.fsdecode(files) + "/")
print("done")
